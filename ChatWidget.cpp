#include "ChatWidget.h"
#include "client.h"
#include <QListView>
#include <QStringListModel>
#include <QLineEdit>
#include <QPushButton>

const int mainWidth = 600;
const int mainHeght = 900;

ChatWidget::ChatWidget(const QString& name, QSharedPointer<Client> client, QWidget * parent)
	: QWidget(parent)
	,_client(std::move(client))
{
	CreateUi();
	setWindowTitle(name);
	resize(mainWidth, mainHeght);
	setAttribute(Qt::WA_DeleteOnClose, true);
}

ChatWidget::~ChatWidget()
{
	_client->Disconnect();
}

void ChatWidget::CreateUi()
{
	_historyView = new QListView(this);
	_history = new ChatModel(_history);
	connect(_client.data(), &Client::TextMessageReceived, _history, &ChatModel::AppendMessage);

	_historyView->setModel(_history);
	_historyView->move(0, 0);

	_input = new QLineEdit(this);
	sendButton = new QPushButton("Send", this);
	connect(sendButton, &QPushButton::clicked, this, [&]()
	{
		_client->SendMessage(_input->text());
		_input->clear();
	});
}

void ChatWidget::resizeEvent(QResizeEvent* e)
{
	_historyView->resize(this->width(), this->height() - 100);

	_input->resize(this->width() - 100, 40);
	_input->move(_historyView->x(), _historyView->y() + _historyView->height() + 5);

	sendButton->resize(100, 40);
	sendButton->move(_input->x() + _input->width(), _input->y());
}

ChatModel::ChatModel(QObject* parent)
	: QAbstractListModel(parent)
{
}

int ChatModel::rowCount(const QModelIndex& parent) const
{
	return _history.size();
}

QVariant ChatModel::data(const QModelIndex& index, int role) const
{
	if (!index.isValid())
	{
		return QVariant();
	}

	if (index.row() >= _history.size())
	{
		return QVariant();
	}

	if (role == Qt::DisplayRole)
	{
		return _history[index.row()];
	}

	return QVariant();
}

void ChatModel::AppendMessage(const QString& message)
{

	_history.push_back(message);
	const auto historySize = _history.size();
	Q_EMIT dataChanged(index(0, historySize), index(0, historySize));
}
