#include <QApplication>
#include "bootstrapper.h"
#include "client.h"
#include "Registrar.h"
#include "controlwindget.h"

int main(int argc, char *argv[])
{
	Communication::RegisterTypes();

	QApplication a(argc, argv);

	Bootstrapper bootstrapper;
	bootstrapper.CreateDummyServer();

	ControlWindget controlWidget(bootstrapper._server, bootstrapper.CreateDummyClientFactory());
	controlWidget.show();

	return a.exec();
}
