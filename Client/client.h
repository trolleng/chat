#pragma once

#include "client_global.h"
#include "IChatClient.h"

class CLIENT_EXPORT Client: public QObject
{
	Q_OBJECT
public:
	Client(const QString& name, QSharedPointer<IChatClient> chatClient);
	~Client();

	void Connect();
	void Disconnect();

	void SendMessage(const QString& message);

Q_SIGNALS:
	void TextMessageReceived(const QString& message);

public Q_SLOTS:
	void OnConnected();
	void OnDisconnected();
	void OnMessageReceived(QSharedPointer<Message> message);
private:
	QSharedPointer<IChatClient> _chatClient;
	QString _name;
};
