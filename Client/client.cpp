#include "client.h"

Client::Client(const QString& name, QSharedPointer<IChatClient> chatClient)
: _chatClient(std::move(chatClient))
	, _name(name)
{
	connect(_chatClient.data(), &IChatClient::OnConnected, this, &Client::OnConnected);
	connect(_chatClient.data(), &IChatClient::OnDisconnected, this, &Client::OnDisconnected);
	connect(_chatClient.data(), &IChatClient::MessageReceived, this, &Client::OnMessageReceived);
}

Client::~Client()
{
	Disconnect();
}

void Client::Connect()
{
	_chatClient->Connect();
}

void Client::Disconnect()
{
	_chatClient->Disconnect();
}

void Client::SendMessage(const QString& message)
{
	Message msg;
	msg.Command = "text";
	msg.Data = message.toUtf8();
	msg.Origin = _name;

	_chatClient->SendMessage(msg);
}

void Client::OnConnected()
{
	qDebug() << Q_FUNC_INFO << "Client connected: " << _name;
}

void Client::OnDisconnected()
{

}

void Client::OnMessageReceived(QSharedPointer<Message> message)
{
	if (message->Command == "text")
	{
		Q_EMIT TextMessageReceived(QString("%0: %1").arg(message->Origin).arg(QString(message->Data)));
	}
}
