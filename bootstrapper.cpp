#include "bootstrapper.h"
#include "DummyTransport.h"
#include "Server.h"
#include "DummyChatClient.h"
#include "client.h"
#include "ClientFactory.h"

void Bootstrapper::CreateDummyServer()
{
	auto transport  = QSharedPointer<DummyTransport>::create();
	_serverTransport = transport;

	_server = QSharedPointer<Server>::create(_serverTransport);
	
	QObject::connect(transport.data(), &DummyTransport::OnNewConnection, _server.data(), &Server::OnNewConnection);
	QObject::connect(transport.data(), &DummyTransport::OnClientDisconnected, _server.data(), &Server::OnDisconnect);
	QObject::connect(transport.data(), &DummyTransport::OnCommand, _server.data(), &Server::OnNewMessage);

	_server->Start();
}

QSharedPointer<IClientFactory> Bootstrapper::CreateDummyClientFactory() const
{
	return QSharedPointer<DummyClientFactory>::create(_serverTransport);
}
