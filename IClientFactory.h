#pragma once
#include "client.h"

class IClientFactory
{
public:
	virtual QSharedPointer<Client> CreateClient(const QString& name) = 0;
};
