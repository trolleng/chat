#include "controlwindget.h"
#include <QListView>
#include <QPushButton>
#include "IClientFactory.h"
#include "Server.h"
#include "ChatWidget.h"

const int mainWidth = 600;
const int mainHeght = 900;

ControlWindget::ControlWindget(QSharedPointer<Server> server, QSharedPointer<IClientFactory> clientFactory)
	: QWidget(nullptr)
	, _server(server)
	, _clientFactory(clientFactory)
{
	CreateUi();
	setWindowTitle("Clients list");
	resize(mainWidth, mainHeght);
}

ControlWindget::~ControlWindget()
{
}

void ControlWindget::CreateUi()
{
	_clientsView = new QListView(this);
	ClientListModel* model = new ClientListModel(_server, _clientsView);
	connect(_server.data(), &Server::ClientsCountChanged, model, &ClientListModel::Update);

	_clientsView->setModel(model);

	_clientsView->move(0, 0);

	_addClienButton = new QPushButton("Add Client", this);
	connect(_addClienButton, &QPushButton::clicked, this, [&]()
	{
		static int index = 0;
		auto clientName = QString("Client_%0").arg(index);
		auto client = _clientFactory->CreateClient(clientName);
		client->Connect();
		auto clientUI = new ChatWidget(clientName, client);
		clientUI->show();
		index++;
	});
}

void ControlWindget::resizeEvent(QResizeEvent* e)
{
	_clientsView->resize(this->width(), this->height() - 100);
	_addClienButton->move(10, _clientsView->y() + _clientsView->height() + 10);
}

int ClientListModel::rowCount(const QModelIndex& parent) const
{
	return _server->Clients().count();
}

QVariant ClientListModel::data(const QModelIndex& index, int role) const
{
	if (!index.isValid())
	{
		return QVariant();
	}

	if (index.row() >= _server->Clients().size())
	{
		return QVariant();
	}

	if (role == Qt::DisplayRole)
	{
		return _server->Clients()[index.row()];
	}
	
	return QVariant();
}

void ClientListModel::Update()
{
	const auto size = _server->Clients().size();
	Q_EMIT dataChanged(index(0, size), index(0, size));
}
