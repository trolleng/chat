#pragma once

#include <QVector>

#include <QDebug>
#include "BinarySerializer.h"

template<class T>
class StreamSerializer
{
public:
    static QByteArray Serialize(const T& message)
    {
        QByteArray data = Serialization::BinarySerializer::Serialize<T>(&message);

        QByteArray packet;
        QDataStream sizeStream(&packet, QIODevice::WriteOnly);
        sizeStream.setVersion(QDataStream::Qt_5_8);
        sizeStream << data.size();

        packet.append(data);
        return packet;
    }

    QVector<QSharedPointer<T>> Deserialize(const QByteArray& buffer)
    {
        QVector<QSharedPointer<T>> result;

        int bufferOffset = 0;

        do
        {
            if (_awaitedSize == 0)
            {
                int bytesToRead = sizeof(_awaitedSize) - _accumulator.size();
                _accumulator.append(buffer.mid(bufferOffset, bytesToRead));
                bufferOffset = qMin(buffer.size(), int(bufferOffset + bytesToRead));

                if (_accumulator.size() >= sizeof(_awaitedSize))
                {
                    QDataStream stream(_accumulator);
                    stream.setVersion(QDataStream::Qt_5_8);
                    stream >> _awaitedSize;

                    _accumulator.clear();
                }
            }
            else if (_awaitedSize > _accumulator.size() + buffer.size() - bufferOffset)
            {
                _accumulator.append(buffer.mid(bufferOffset));
                break;
            }
            else
            {
                int bytesToRead = _awaitedSize - _accumulator.size();
                _accumulator.append(buffer.mid(bufferOffset, bytesToRead));
                bufferOffset += bytesToRead;

                qDebug() << Q_FUNC_INFO << " packet Assembled size is: " << _accumulator.size();

                QSharedPointer<T> message(Serialization::BinarySerializer::Deserialize<T>(_accumulator));

                result.push_back(message);
                _accumulator.clear();
                _awaitedSize = 0;
            }
        }
        while (buffer.size() > bufferOffset);

        return result;
    }

private:
	int _awaitedSize = 0;
	QByteArray _accumulator;
};
