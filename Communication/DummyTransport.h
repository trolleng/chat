#pragma once
#include "IServerTransport.h"
#include "IChatClient.h"

class COMMUNICATION_EXPORT DummyTransport: public IServerTransport
{
	Q_OBJECT
public:
	DummyTransport();
	bool Start() override;
	void Stop() override;
Q_SIGNALS:
	void OnNewConnection(const QString& name, IChatClient* connection);
	void OnClientDisconnected(const QString& name, IChatClient* connection);
 public Q_SLOTS:
	void OnConnection(const QString& name);
	void OnDisconnect(const QString& name);
};
