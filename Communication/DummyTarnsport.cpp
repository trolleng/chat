#include "DummyTransport.h"


DummyTransport::DummyTransport()
{

}

bool DummyTransport::Start()
{
	return true;
}

void DummyTransport::Stop()
{
}

void DummyTransport::OnConnection(const QString& name)
{
	Q_EMIT OnNewConnection(name, static_cast<IChatClient*>(sender()));
}

void DummyTransport::OnDisconnect(const QString& name)
{
	Q_EMIT OnClientDisconnected(name, static_cast<IChatClient*>(sender()));
}
