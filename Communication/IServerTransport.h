#pragma once
#include <QObject>
#include "communication_global.h"

class Message;

class COMMUNICATION_EXPORT IServerTransport: public QObject
{
	Q_OBJECT
public:
	virtual bool Start() = 0;
	virtual void Stop() = 0;

	Q_SIGNALS:
	void OnCommand(QSharedPointer<Message> message);
	void ClientConnected(const QString& name);
};
