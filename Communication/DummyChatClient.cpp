#include "DummyChatClient.h"

DummyChatClient::DummyChatClient(const QString& name, QSharedPointer<IServerTransport> serverTransport)
	: _name(name)
	, _serverTransport(serverTransport)
{
}

bool DummyChatClient::Connect()
{
	Q_EMIT ConnectToServer(_name);

	_connected = true;
	return true;
}

void DummyChatClient::Disconnect()
{
	Q_EMIT DisconnetFromServer(_name);
}

void DummyChatClient::SendMessage(const Message& message)
{
	const auto serverMessage =QSharedPointer<Message>::create(message);
	_serverTransport->OnCommand(serverMessage);
}
