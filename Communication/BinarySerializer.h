#pragma once
#include <QMetaProperty>
#include <QDataStream>
#include "GenericSerializer.h"


class QByteArray;

namespace Serialization
{
class BinarySerializer : protected GenericSerializer
{
	static const QStringList IgnoredProperties;
	static const QLatin1String QVariantTypeName;
public:
	BinarySerializer()
	{
	}

	~BinarySerializer()
	{
	}

	template<class T>
	static QByteArray Serialize(const T* object)
	{
		QByteArray data;
		QDataStream stream(&data, QIODevice::WriteOnly);
		QVariantMap properties = SerializeObject(object);

		stream.setVersion(QDataStream::Qt_5_10);
		stream << properties;
		return data;
	}

	template<class T>
	static QByteArray SerializeBase64(const T* object)
	{
		QByteArray data = Serialize<T>(object);
		return data.toBase64();
	}

	template<class T>
	static T* Deserialize(const QByteArray& data)
	{
		QVariantMap properties;
		QDataStream stream(data);
		stream.setVersion(QDataStream::Qt_5_8);

		stream >> properties;

		T* result = new T();

		DeserializeObject(result, properties);

		return result;
	}

	template<class T>
	static T* DeserializeBase64(const QByteArray& data)
	{
		QByteArray object = QByteArray::fromBase64(data);
		return Deserialize<T>(object);
	}
};
}
