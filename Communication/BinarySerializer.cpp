#include "BinarySerializer.h"
#include <QString>
#include <QVariant>

using namespace Serialization;

const QStringList BinarySerializer::IgnoredProperties = QStringList(QString(QLatin1String("objectName")));
const QLatin1String BinarySerializer::QVariantTypeName = QLatin1String("QVariant");
