#include "Registrar.h"
#include "Message.h"
#include "QMetaType"

void Communication::RegisterTypes()
{
	qRegisterMetaType<Message>();
	qRegisterMetaType<QSharedPointer<Message>>();
}
