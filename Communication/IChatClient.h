#pragma once

#include <QObject>
#include "communication_global.h"
#include <QString>
#include "Message.h"

class QLocalSocket;

class COMMUNICATION_EXPORT IChatClient : public QObject
{
	Q_OBJECT
public:
	virtual ~IChatClient() = default;

	virtual bool Connect() = 0;
	virtual void Disconnect() = 0;
	virtual void SendMessage(const Message& message) = 0;

	Q_SIGNALS:
	void OnConnected();
	void OnDisconnected();
	void MessageReceived(QSharedPointer<Message> message);

	void OnError(QString socketError);
};
