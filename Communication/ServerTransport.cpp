#include "ServerTransport.h"
#include "QLocalServer"
#include <QLocalSocket>
#include "Message.h"
#include "StreamSerializer.h"

static StreamSerializer<Message> Serializer;

ServerTransport::ServerTransport(const QString& serverName, bool allowMultipleConnections)
	: _localServer(Q_NULLPTR)
	, _serverName(IPCCHANELNAME(serverName))
	, _allowMultipleConnections(allowMultipleConnections)
{
}

ServerTransport::ServerTransport(QLocalServer* server, const QString& serverName, bool allowMultipleConnections)
	: _localServer(server)
	, _serverName(serverName)
	, _allowMultipleConnections(allowMultipleConnections)
{
}

ServerTransport::~ServerTransport()
{
	StopServer();
}

bool ServerTransport::Start()
{
	bool result = true;
	if (_localServer != Q_NULLPTR)
	{
		qInfo() << Q_FUNC_INFO << "Server " << _serverName << "has already been started";
	}
	else
	{
		QMutexLocker locker(&_mutex);
		if (_localServer == Q_NULLPTR)
		{
			_localServer = new QLocalServer(this);
			_localServer->setSocketOptions(QLocalServer::WorldAccessOption);

			connect(_localServer, &QLocalServer::newConnection, this, &ServerTransport::OnNewConnection);

			result = _localServer->listen(_serverName);
			if (result)
			{
				qDebug() << Q_FUNC_INFO << " server: '" << _serverName << "' started.";
			}
			else
			{
				qWarning() << Q_FUNC_INFO << " can not listen '" << _serverName << "'";
			}
		}
	}

	return result;
}

void ServerTransport::Stop()
{
	StopServer();
}

void ServerTransport::OnNewConnection()
{
	if (_connectionCount > 0 && !_allowMultipleConnections)
	{
		qInfo() << Q_FUNC_INFO << _serverName << "Multiple connections not allowed";
		return;
	}

	_connectionCount++;

	qDebug() << Q_FUNC_INFO << _serverName << "'";

	QLocalSocket* socket = _localServer->nextPendingConnection();

	connect(socket, &QLocalSocket::disconnected, this, &ServerTransport::OnDisconnected);
	connect(socket, &QLocalSocket::readyRead, this, &ServerTransport::OnReadyRead);

	Q_EMIT NewConnection(socket);
	Q_EMIT ClientConnected(_serverName);

	qDebug() << Q_FUNC_INFO << _serverName << " service connected";
}

void ServerTransport::OnReadyRead()
{
	QLocalSocket* connection = qobject_cast<QLocalSocket*>(sender());

	auto data = connection->readAll();

	QVector<QSharedPointer<Message>> messages = Serializer.Deserialize(data);
	for (auto message : messages)
	{
		Q_EMIT OnCommand(message);
		qDebug() << Q_FUNC_INFO << " '" << _serverName << "' received command " << message->Command;
	}
}

void ServerTransport::OnDisconnected()
{
	QLocalSocket* connection = qobject_cast<QLocalSocket*>(sender());
	connection->deleteLater();
	_connectionCount--;

	qDebug() << Q_FUNC_INFO << " '" << _serverName << "' client service disconnected";
}

void ServerTransport::StopServer()
{
	qDebug() << Q_FUNC_INFO << " server: '" << _serverName << "' stopped";

	QMutexLocker locker(&_mutex);
	if (_localServer != Q_NULLPTR)
	{
		_localServer->close();
		_localServer->deleteLater();
		_localServer = Q_NULLPTR;
		_connectionCount = 0;
	}
}
