#pragma once
#include "IChatClient.h"
#include "QLocalSocket"
#include <QTimer>


class COMMUNICATION_EXPORT ChatClient: public IChatClient
{
	Q_OBJECT
public:
	explicit ChatClient(const QString& serverName, bool autoReconnect = true);
	~ChatClient();

	bool Connect() override;
	void Disconnect() override;
	void SendMessage(const Message& message) override;

	Q_SIGNALS:
	void ReadyRead(QLocalSocket*);

private Q_SLOTS:
	void OnStateChanged(QLocalSocket::LocalSocketState socketState);
	void OnError(QLocalSocket::LocalSocketError socketError) const;
	void OnReadyRead();

	void Reconnect();
private:
	const int RECONNECT_TIMEOUT = 1000;

	QLocalSocket* _localSocket;
	const QString _serverName;
	QTimer _reconnectTimer;
	const bool _autoReconnect;

	bool _connected;
};
