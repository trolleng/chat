#pragma once

#include <QtCore/qglobal.h>

#define _BUILD_AS_STATIC_LIB

#ifdef  _BUILD_AS_STATIC_LIB
#define COMMUNICATION_EXPORT 
#else
#ifdef COMMUNICATION_LIB
# define COMMUNICATION_EXPORT Q_DECL_EXPORT
#else
# define COMMUNICATION_EXPORT Q_DECL_IMPORT
#endif
#endif


#ifdef Q_OS_WIN32
#define IPCCHANELNAME(s) QString("\\\\.\\pipe\\" + s)
#else
#define IPCCHANELNAME(s) QString("/tmp/" + s)
#endif

