#pragma once
#include "IServerTransport.h"
#include <QMutex>

class QLocalSocket;
class QLocalServer;

class COMMUNICATION_EXPORT ServerTransport :
	public IServerTransport
{
	Q_OBJECT
public:
	ServerTransport(const QString& serverName, bool allowMultipleConnections = false);
	ServerTransport(QLocalServer* server, const QString& ServerTransport, bool allowMultipleConnections = false);
	~ServerTransport();

	bool Start() override;
	void Stop() override;

	Q_SIGNALS:
	void NewConnection(QLocalSocket*);

protected Q_SLOTS:
	void OnNewConnection();
	void OnReadyRead();
	void OnDisconnected();

protected:
	void StopServer();

	QLocalServer* _localServer;
	int _connectionCount = 0;
	QString _serverName;
	bool _allowMultipleConnections = false;

	QMutex _mutex;
};
