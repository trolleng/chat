#pragma once

#include <QVariant>
#include <QMetaProperty>
#include <QDebug>
#include <QRegularExpression>
#include <QMap>

namespace Serialization
{
class ISerializableList : public QObject
{
Q_OBJECT
public:
	ISerializableList() = default;

	ISerializableList(const ISerializableList&)
	{
	}

	ISerializableList& operator=(const ISerializableList&)
	{
		return *this;
	}

	virtual void append(QObject*) = 0;

	virtual int count() const = 0;
	virtual QObject* get(int index) const = 0;
};

class GenericSerializer
{
public:
	static QVariantMap SerializeObject(const QObject* object)
	{
		QVariantMap properties;

		const QMetaObject* metaobject = object->metaObject();
		int count = metaobject->propertyCount();
		for (int i = 1; i < count; ++i)
		{
			auto metaproperty = metaobject->property(i);

			if (!metaproperty.isReadable())
			{
				continue;
			}
			auto name = metaproperty.name();
			QLatin1String propertyName(name);

			QVariant value = metaproperty.read(object);
			if (value.type() == QMetaType::User)
			{
				if (metaproperty.isEnumType())
				{
					value = value.toInt();
				}
				else if (value.canConvert<QObject*>())
				{
					QObject* propertyObject = value.value<QObject*>();
					ISerializableList* propertyList = qobject_cast<ISerializableList*>(propertyObject);
					if (propertyList)
					{
						QVariantList childProperties;
						int listCount = propertyList->count();
						for (int elementIndex = 0; elementIndex < listCount; ++elementIndex)
						{
							QObject* element = propertyList->get(elementIndex);
							auto propertyMap = SerializeObject(element);
							childProperties.append(propertyMap);
						}
						value = childProperties;
					}
					else
					{
						auto propertyMap = SerializeObject(propertyObject);
						value = propertyMap;
					}
				}
			}

			properties[propertyName] = value;
		}
		return properties;
	}

	static void DeserializeObject(QObject* object, const QVariantMap& properties)
	{
		const QMetaObject* metaObject = object->metaObject();
		int propertyCount = metaObject->propertyCount();
		for (int propertyIndex = 1; propertyIndex < propertyCount; ++propertyIndex)
		{
			const QMetaProperty property = metaObject->property(propertyIndex);
			if (!property.isWritable())
			{
				continue;
			}
			QString propertyName = property.name();

			auto propertyIterator = properties.find(propertyName);
			if (propertyIterator == properties.end())
			{
				qWarning() << Q_FUNC_INFO << ": no property with name " << propertyName;
				continue;
			}
			QVariant propertyValue = propertyIterator.value();

			if (property.isEnumType())
			{
				propertyValue = propertyValue.toInt();
			}
			if (property.write(object, propertyValue))
			{
				continue;
			}

			//Failed to write, so this is non-trivial type. Assume there is a pointer to compound type.
			auto typeName = property.typeName();
			int propertyObjectTypeID = TypeWithoutPointer(typeName);
			if (propertyObjectTypeID == QMetaType::UnknownType)
			{
				qWarning() << Q_FUNC_INFO << "don't know type of property " << propertyName << ": " << typeName;
				continue;
			}
			QObject* propertyObject = static_cast<QObject*>(QMetaType::create(propertyObjectTypeID));
			propertyObject->setParent(object);
			if (!property.write(object, QVariant::fromValue(propertyObject)))
			{
				qWarning() << Q_FUNC_INFO << "failed to store object in property " << propertyName;
				continue;
			}

			switch (propertyValue.type())
			{
				case QMetaType::QVariantMap:
				{
					DeserializeObject(propertyObject, propertyValue.toMap());
					break;
				}
				case QMetaType::QVariantList:
				{
					ISerializableList* list = qobject_cast<ISerializableList*>(propertyObject);
					QVariantList propertyList = propertyValue.toList();
					int elementTypeID = TypeOfListElement(propertyObjectTypeID);
					if (elementTypeID == QMetaType::UnknownType)
					{
						qWarning() << Q_FUNC_INFO << "can't infer list element type from " << QMetaType::typeName(propertyObjectTypeID);
						continue;
					}
					Q_FOREACH(const QVariant& element, propertyList)
					{
						if (!element.canConvert<QVariantMap>())
						{
							qWarning() << Q_FUNC_INFO << "property " << propertyName << ": array element is not object";
							continue;
						}
						QObject* elementObject = static_cast<QObject*>(QMetaType::create(elementTypeID));
						list->append(elementObject);
						DeserializeObject(elementObject, element.toMap());
					}
					break;
				}
				default:
					qWarning() << Q_FUNC_INFO << "don't know how to deserialize object for property " << propertyName;
			}
		}
	}

	static int TypeWithoutPointer(QString typeName)
	{
		static const QChar asterisk = '*';
		int result;

		if (typeName.endsWith(asterisk))
		{
			QString newType = typeName.left(typeName.size() - 1);
			result = QMetaType::type(newType.toLatin1());
		}
		else
		{
			result = QMetaType::UnknownType;
		}
		return result;
	}

	static int TypeOfListElement(int listType)
	{
		static const QRegularExpression regexp("<([^>]*)>");

		QString listTypeName = QMetaType::typeName(listType);
		QRegularExpressionMatch match = regexp.match(listTypeName);
		QString elementTypeName = match.captured(1);
		return TypeWithoutPointer(elementTypeName);
	}
};
}
