#pragma once
#include "IChatClient.h"
#include "IServerTransport.h"

class COMMUNICATION_EXPORT DummyChatClient: public IChatClient
{
	Q_OBJECT
public:
	DummyChatClient(const QString& name, QSharedPointer<IServerTransport> serverTransport);

	bool Connect() override;
	void Disconnect() override;
	void SendMessage(const Message& message) override;
Q_SIGNALS:
	void ConnectToServer(const QString& name);
	void DisconnetFromServer(const QString& name);

protected:
	QString _name;
	QSharedPointer<IServerTransport> _serverTransport;
	bool _connected = false;

};
