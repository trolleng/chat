#pragma once
#include <QObject>
#include <QDebug>
#include "communication_global.h"

class COMMUNICATION_EXPORT Message: public QObject
{
	Q_OBJECT
	Q_PROPERTY(QString Command MEMBER Command)
	Q_PROPERTY(QString Origin MEMBER Origin)
	Q_PROPERTY(QByteArray Data MEMBER Data)
public:
	QString Command;
	QString Origin;
	QByteArray Data;

	Message() = default;

	Message(const Message& other)
		: Command(other.Command)
		, Origin(other.Origin)
		, Data(other.Data)
	{
	}

};

Q_DECLARE_METATYPE(Message)
