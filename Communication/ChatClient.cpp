#include "ChatClient.h"

#include <QDebug>
#include "Message.h"
#include <QByteArray>
#include "StreamSerializer.h"

ChatClient::ChatClient(const QString& serverName, bool autoReconnect)
	: _localSocket(Q_NULLPTR)
	, _serverName(IPCCHANELNAME(serverName))
	, _autoReconnect(autoReconnect)
	, _connected(false)
{
	_reconnectTimer.setInterval(RECONNECT_TIMEOUT);
	connect(&_reconnectTimer, &QTimer::timeout, this, &ChatClient::Reconnect);
}

ChatClient::~ChatClient()
{
}

bool ChatClient::Connect()
{
	if (_localSocket != Q_NULLPTR)
	{
		if (_localSocket->state() == QLocalSocket::ConnectedState)
		{
			qWarning() << Q_FUNC_INFO << "ServiceClient" << _serverName << "already connected.";
			return true;
		}
		_localSocket->deleteLater();
	}

	_localSocket = new QLocalSocket();
	_localSocket->setServerName(_serverName);

	connect(_localSocket, &QLocalSocket::stateChanged, this, &ChatClient::OnStateChanged);
	connect(_localSocket, static_cast<void(QLocalSocket::*)(QLocalSocket::LocalSocketError)>(&QLocalSocket::error), this, &ChatClient::OnError);
	connect(_localSocket, &QLocalSocket::readyRead, this, &ChatClient::OnReadyRead);

	_localSocket->connectToServer(QLocalSocket::ReadWrite);

	bool connected = _localSocket->waitForConnected();
	if (!connected)
	{
		qWarning() << "Connect to local server '" << _serverName << "' failed. ";
		if (_autoReconnect)
		{
			_reconnectTimer.start();
		}
	}
	else
	{
		qDebug() << Q_FUNC_INFO << " '" << _serverName << "' Connected";
		Q_EMIT OnConnected();
	}
	return connected;
}

void ChatClient::Disconnect()
{
	if (_localSocket)
	{
		_localSocket->disconnectFromServer();
		disconnect(_localSocket, &QLocalSocket::stateChanged, this, &ChatClient::OnStateChanged);
		disconnect(_localSocket, static_cast<void(QLocalSocket::*)(QLocalSocket::LocalSocketError)>(&QLocalSocket::error), this, &ChatClient::OnError);
		disconnect(_localSocket, &QLocalSocket::readyRead, this, &ChatClient::OnReadyRead);

		_localSocket->deleteLater();
		_localSocket = Q_NULLPTR;
	}

	_reconnectTimer.stop();

	Q_EMIT OnDisconnected();
	qDebug() << Q_FUNC_INFO << " '" << _serverName << "' Disconnect";
}

void ChatClient::SendMessage(const Message& message)
{
	if (_localSocket == Q_NULLPTR)
	{
		qWarning() << Q_FUNC_INFO << "no socket:" << _serverName << message.Command;
		return;
	}
	QByteArray data = StreamSerializer<Message>::Serialize(message);

	qint64 sentBytes = _localSocket->write(data);
	_localSocket->flush();

	if (sentBytes == -1)
	{
		qWarning() << Q_FUNC_INFO << " Failed to send data: " << _localSocket->errorString();
	}
}

void ChatClient::OnStateChanged(QLocalSocket::LocalSocketState socketState)
{
	qDebug() << Q_FUNC_INFO << " '" << _serverName << "' local socket new state: " << socketState;
	if (socketState == QLocalSocket::LocalSocketState::UnconnectedState)
	{
		if (_connected)
		{
			_connected = false;
			Q_EMIT OnDisconnected();
		}
		else if (!_reconnectTimer.isActive())
		{
			_reconnectTimer.start();
		}
	}
	else if (socketState == QLocalSocket::LocalSocketState::ConnectedState)
	{
		_connected = true;
	}
}

void ChatClient::OnError(QLocalSocket::LocalSocketError socketError) const
{
	qDebug() << Q_FUNC_INFO << " '" << _serverName << "' local socket error: " << socketError;
}

void ChatClient::OnReadyRead()
{
	QLocalSocket* connection = qobject_cast<QLocalSocket*>(sender());
	Q_EMIT ReadyRead(connection);
}

void ChatClient::Reconnect()
{
	qDebug() << Q_FUNC_INFO << _serverName << " try connect";
    _localSocket->connectToServer(_serverName);
	if (_localSocket->waitForConnected())
	{
		qDebug() << Q_FUNC_INFO << " '" << _serverName << "' connected";
		Q_EMIT OnConnected();
		_reconnectTimer.stop();
	}
}
