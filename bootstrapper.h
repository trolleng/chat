#pragma once
#include <memory>
#include <QSharedPointer>
#include "IClientFactory.h"

class QString;
class Server;
class Client;
class QWidget;
class IServerTransport;

class Bootstrapper
{
public:

	void CreateDummyServer();
	QSharedPointer<IClientFactory> CreateDummyClientFactory() const;
	

	QSharedPointer<IServerTransport> _serverTransport;
	QSharedPointer<Server> _server;
};
