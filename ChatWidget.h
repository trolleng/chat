#pragma once
#include <QWidget>
#include <qlistview.h>

class Client;
class Message;
class QPushButton;
class ChatModel;

class ChatWidget: public QWidget
{
	Q_OBJECT
public:
	ChatWidget(const QString& name, QSharedPointer<Client> client, QWidget * parent = nullptr);
	~ChatWidget();

private:
	void CreateUi();
	void resizeEvent(QResizeEvent *e) override;
	QSharedPointer<Client> _client;
	QListView* _historyView = nullptr;
	QLineEdit* _input = nullptr;
	QPushButton* sendButton = nullptr;
	ChatModel* _history = nullptr;
};

class ChatModel : public QAbstractListModel
{
	Q_OBJECT

public:
	ChatModel(QObject* parent);

	int rowCount(const QModelIndex& parent) const override;
	QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
public Q_SLOTS:
	void AppendMessage(const QString& message);

private:
	QStringList _history;
};
