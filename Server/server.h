#pragma once

#include "server_global.h"
#include "IServerTransport.h"
#include "IChatClient.h"
#include <QThread>

class SERVER_EXPORT Server: public QObject
{
	Q_OBJECT
public:
	Server(QSharedPointer<IServerTransport> transport);
	~Server();

	QSharedPointer<IServerTransport> _transport;

	void Start();
	void Stop();

	QList<QString> Clients() const;

Q_SIGNALS:
	void ClientsCountChanged();

public Q_SLOTS:
	void OnNewConnection(const QString& name,IChatClient * client);
	void OnDisconnect(const QString& name, IChatClient * client);

	void OnNewMessage(QSharedPointer<Message> message);
	void SendBroadcast(QSharedPointer<Message> message);
protected:
	QMap<QString, IChatClient*> _clients;
	QThread _workerThread;
};
