#include "Server.h"

Server::Server(QSharedPointer<IServerTransport> transport)
: _transport(std::move(transport)) 
{
	_workerThread.setObjectName("Server thread");
}

Server::~Server()
{
	Stop();
}

void Server::Start()
{
	qDebug() << Q_FUNC_INFO << "Server started";
	this->moveToThread(&_workerThread);
	_workerThread.start();
}

void Server::Stop()
{
	qDebug() << Q_FUNC_INFO << "Server stopped";
	_workerThread.quit();
	_workerThread.wait();
}

QList<QString> Server::Clients() const
{
	return _clients.keys();
}

void Server::OnNewConnection(const QString& name, IChatClient * client)
{
	qDebug() << Q_FUNC_INFO << "Client connected" << name;
	_clients[name] = client;
	Q_EMIT ClientsCountChanged();
}

void Server::OnDisconnect(const QString& name, IChatClient* client)
{
	_clients.remove(name);
	Q_EMIT ClientsCountChanged();
}

void Server::OnNewMessage(QSharedPointer<Message> message)
{
	SendBroadcast(message);
}

void Server::SendBroadcast(QSharedPointer<Message> message)
{
	for (auto client : _clients)
	{
		client->MessageReceived(message);
	}
}
