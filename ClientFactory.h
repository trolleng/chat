#pragma once
#include "IClientFactory.h"
#include "IServerTransport.h"

class DummyClientFactory: public IClientFactory
{
public:
	DummyClientFactory(QSharedPointer<IServerTransport> serverTransport);

	QSharedPointer<Client> CreateClient(const QString& name) override;
	QSharedPointer<IServerTransport> _serverTransport;
};
