#pragma once

#include <QWidget>
#include "bootstrapper.h"
#include <qabstractitemmodel.h>

class Server;
class QListView;
class QPushButton;
class IClientFactory;

class ControlWindget: public QWidget
{
	Q_OBJECT
public:
	ControlWindget(QSharedPointer<Server> server, QSharedPointer<IClientFactory> clientFactory);
	~ControlWindget();

private:
	void CreateUi();
	void resizeEvent(QResizeEvent *e) override;

	QListView * _clientsView;
	QSharedPointer<Server> _server;
	QPushButton *_addClienButton;
	QSharedPointer<IClientFactory> _clientFactory;
};

class ClientListModel : public QAbstractListModel
{
	Q_OBJECT

public:
	ClientListModel(QSharedPointer<Server> server, QObject *parent = nullptr)
		: QAbstractListModel(parent), _server(server) {}

	int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	QVariant data(const QModelIndex &index, int role) const override;

	Qt::ItemFlags flags(const QModelIndex &index) const override
	{
		return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
	}
public Q_SLOTS:
	void Update();
private:
	QSharedPointer<Server> _server;
};
