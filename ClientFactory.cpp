#include "ClientFactory.h"
#include "DummyChatClient.h"
#include "DummyTransport.h"

DummyClientFactory::DummyClientFactory(QSharedPointer<IServerTransport> serverTransport)
: _serverTransport(std::move(serverTransport)) 
{
}

QSharedPointer<Client> DummyClientFactory::CreateClient(const QString& name)
{
	auto chatClient = QSharedPointer<DummyChatClient>::create(name, _serverTransport);
	QObject::connect(chatClient.data(), &DummyChatClient::ConnectToServer, static_cast<DummyTransport*>(_serverTransport.data()), &DummyTransport::OnConnection);
	QObject::connect(chatClient.data(), &DummyChatClient::DisconnetFromServer, static_cast<DummyTransport*>(_serverTransport.data()), &DummyTransport::OnDisconnect);

	return QSharedPointer<Client>::create(name, chatClient);
}
